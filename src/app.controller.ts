import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import {MessagePattern} from "@nestjs/microservices";
import {SendEmailDto} from "./dto/send-email.dto";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @MessagePattern({ cmd: 'email_send' })
  async sendEmail(fields: SendEmailDto): Promise<number> {
    console.log(`HOST: ${process.env.SMTP_HOST}`)
    return this.appService.sendEmail(fields);
  }
}
