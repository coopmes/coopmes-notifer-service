import {IsEmail, IsNotEmpty} from "class-validator";

export class SendEmailDto {
    @IsEmail()
    address: string;

    subject: string;

    text: string;

    html: string;
}
