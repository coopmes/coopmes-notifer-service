import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {MailerModule} from '@nestjs-modules/mailer';
import {PugAdapter} from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import {MongooseModule} from "@nestjs/mongoose";
import {ConfigModule} from "@nestjs/config";

@Module({
    imports: [
        MongooseModule.forRoot('mongodb://172.17.0.1/notifications'),
        ConfigModule.forRoot(),
        MailerModule.forRoot({
            //transport: `smtp://${process.env.SMTP_LOGIN}:${process.env.SMTP_PASSWORD}@${process.env.SMTP_HOST}`,
            transport: {
                //pool: true,
                host: process.env.SMTP_HOST,
                port: process.env.SMTP_PORT,
                secure: true,
                auth: {
                    user: process.env.SMTP_LOGIN,
                    pass: process.env.SMTP_PASSWORD,
                },
                logger: true,
                debug: true,
            },
            defaults: {
                from: `"Support no-reply" <${process.env.SMTP_LOGIN}@ya.ru>`,
            },
            template: {
                dir: __dirname + '/templates',
                adapter: new PugAdapter(),
                options: {
                    strict: true,
                },
            },
        }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
