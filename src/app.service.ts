import {Injectable} from '@nestjs/common';
import {SendEmailDto} from "./dto/send-email.dto";
import {MailerService} from "@nestjs-modules/mailer";

@Injectable()
export class AppService {

    constructor(private readonly mailerService: MailerService) {
    }

    getHello(): string {
        return 'Hello World!';
    }

    sendEmail(fields: SendEmailDto) {
        console.log(fields);

        return this
            .mailerService
            .sendMail({
                // from: `"Support" <${process.env.SMTP_LOGIN}>`,
                to: fields.address,
                subject: fields.subject,
                text: fields.text,
                html: fields.html
            })
    }
}
